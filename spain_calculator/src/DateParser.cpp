// SPDX-FileCopyrightText: 2022 Simon Barth <simon.barth@gmx.de>
//
// SPDX-License-Identifier: GPL-3.0-only

#include "DateParser.hpp"

#include <chrono>
#include <filesystem>
#include <fstream>
#include <numeric>
#include <optional>
#include <sstream>
#include <string>
#include <string_view>
#include <vector>

#include <date/date.h>

namespace spain_calculator {

// Implementations at the end of this file
namespace detail {

/**
* @brief Retrieve all days listed in a single database file
*
* @param[in] database_file: A std::filesystem::path to a single database file
*
* @returns A std::vector containing all dates in database_file
*/
auto get_days_worked(const std::filesystem::path database_file) -> std::vector<std::chrono::year_month_day>;

}

auto parse(std::string_view date_as_text) -> std::optional<std::chrono::year_month_day> {
	std::istringstream date_stream{std::string{date_as_text}};
	std::chrono::sys_days date;

	// puts 0 into date on invalid date format
	date_stream >> date::parse("%F", date);

	if( date_stream.fail() ) {
		return std::nullopt;
	}

	return date;
}


auto get_database_files(const std::filesystem::path base_directory) -> std::vector<std::filesystem::path>
{
	std::vector<std::filesystem::path> database_files{};
	if (not std::filesystem::is_directory(base_directory)) {
		return database_files;
	}

	// Not doing recursion, using any regular file
	for (auto const& dir_entry : std::filesystem::directory_iterator{base_directory}) {
		if (dir_entry.is_regular_file()) {
				database_files.push_back(dir_entry.path());
		}
	}

	return database_files;
}


auto get_all_days_worked(const std::vector<std::filesystem::path> database_files) -> std::vector<std::chrono::year_month_day> {

	// Flatten std::vector of std::vectors of std::chrono::year_month_day into
	// std::vector of std::chrono::year_month_day
	const auto flatten = [](auto& dest, auto& src) {
		dest.insert(dest.end(), src.begin(), src.end());
		return dest;
	};

	std::vector<std::vector<std::chrono::year_month_day>> result;

	std::transform(database_files.cbegin(), database_files.cend(),
								std::back_inserter(result),
								detail::get_days_worked);

	return std::reduce(result.begin(), result.end(),
										decltype(result)::value_type{},
										flatten);

}


// Functions only used as implementation details
namespace detail {

auto get_days_worked(const std::filesystem::path database_file) -> std::vector<std::chrono::year_month_day> {
	std::vector<std::chrono::year_month_day> result;
	std::ifstream database_stream{database_file};

	for (std::string line ; std::getline(database_stream, line);) {
		auto parsed_date = spain_calculator::parse(line);
		if (parsed_date) {
			result.push_back(parsed_date.value());
		}
	}

	return result;
}

}
}

