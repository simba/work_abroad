// SPDX-FileCopyrightText: 2022 Simon Barth <simon.barth@gmx.de>
//
// SPDX-License-Identifier: GPL-3.0-only

#include "DateParser.hpp"
#include "mobile_work_abroad_calculator/Calculator.hpp"

#include <chrono>
#include <cstdlib>
#include <filesystem>
#include <iomanip>
#include <iostream>
#include <string>

#include <CLI/CLI11.hpp>

// Nicely formatted output of results
auto output_results(const std::chrono::days leftover_days, const std::chrono::year_month_day next_possible_working_day) -> void
{
	std::cout << "Working days left: " << leftover_days.count() << std::endl;
	std::cout
		<< "Next possible working day: "
		<< int(next_possible_working_day.year())
		<< "-"
		<< std::setw(2) << std::setfill('0')
		<< unsigned(next_possible_working_day.month())
		<< "-"
		<< std::setw(2) << std::setfill('0')
		<< unsigned(next_possible_working_day.day())
		<< std::endl;
}

auto main(int argc, char* argv[]) -> int
{
	CLI::App spain_calculator{"Calculate leftover working days abroad and next possible date to work abroad"};

	std::filesystem::path working_days_folder;
	spain_calculator.add_option("--working-days-folder", working_days_folder, "Folder containing files with working day entries")->check(CLI::ExistingDirectory);

	std::string target_date_string;
	spain_calculator.add_option("--target-date", target_date_string, "Do the calculation for this day");
	// TODO: Figure out how to verify or even verify and transform from string to std::chrono::sys_days

	std::chrono::days contingent_working_days;
	spain_calculator.add_option("--contingent", contingent_working_days, "How many days can be worked per 12 month period")->check(CLI::PositiveNumber);

	CLI11_PARSE(spain_calculator, argc, argv);

	const auto parsed_date = spain_calculator::parse(target_date_string);
	if (not parsed_date) {
		std::cerr << "Target date "
							<< target_date_string
							<< " couldn't be read, using today's date instead."
							<< std::endl;
	}

	const auto today = std::chrono::time_point_cast<std::chrono::days>(std::chrono::system_clock::now());
	const auto target_date = parsed_date.value_or(today);

	const auto database_files = spain_calculator::get_database_files(working_days_folder);
	const auto days_worked = spain_calculator::get_all_days_worked(database_files);

	const Calculator calc{contingent_working_days, target_date, days_worked};

	output_results(calc.LeftoverDays(), calc.NextPossibility());

	return EXIT_SUCCESS;
}

