// SPDX-FileCopyrightText: 2022 Simon Barth <simon.barth@gmx.de>
//
// SPDX-License-Identifier: GPL-3.0-only

#ifndef _DATE_PARSER_
#define _DATE_PARSER_

#include <chrono>
#include <filesystem>
#include <optional>
#include <string_view>
#include <vector>

namespace spain_calculator {

/**
* @brief Parse a date
*
* @param[in] date_as_text: A string view representing a date in YYYY-MM-DD format
*
* @returns An optional holding a corresponding std::chrono::year_month_day
* 				 object if parsing was succesful
*/
auto parse(std::string_view date_as_text) -> std::optional<std::chrono::year_month_day>;

/**
* @brief Find all files containing dates in a directory
*
* Any regular file will be regarded a database file. Files are not looked for
* recursively.
*
* @param[in] base_directory: The directory to search for database files
*
* @returns A std::vector containing all database files in base_directory.
*/
auto get_database_files(const std::filesystem::path base_directory) -> std::vector<std::filesystem::path>;

/**
* @brief Retrieve all days listed in several database files
*
* @param[in] database_files: A std::vector of std::filesystem::path of database files
*
* @returns A std::vector containing all dates from all files in database_files
*/
auto get_all_days_worked(const std::vector<std::filesystem::path> database_files) -> std::vector<std::chrono::year_month_day>;

}
#endif

