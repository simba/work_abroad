// SPDX-FileCopyrightText: 2022 Simon Barth <simon.barth@gmx.de>
//
// SPDX-License-Identifier: GPL-3.0-only

#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include "DateParser.hpp"

#include <chrono>

SCENARIO( "Parse date from string", "[SpainCalculator]") {
	using namespace std::string_view_literals;
	GIVEN( "A valid date string in YYYY-MM-DD format" ) {
		constexpr const auto date_as_text = "2022-03-22"sv;
		WHEN( "The string is parsed" ) {
			const auto date = spain_calculator::parse(date_as_text);
			THEN( "That date is returned" ) {
				using namespace std::chrono_literals;
				REQUIRE( date.value() == 2022y/03/22 );
			}
		}
	}
	GIVEN( "An empty string" ) {
		constexpr const auto date_as_text = ""sv;
		WHEN( "The string is parsed" ) {
			const auto  date = spain_calculator::parse(date_as_text);
			THEN( "An invalid date is returned" ) {
				using namespace std::chrono_literals;
				REQUIRE( not date );
			}
		}
	}
}

