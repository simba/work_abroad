// SPDX-FileCopyrightText: 2022 Simon Barth <simon.barth@gmx.de>
//
// SPDX-License-Identifier: GPL-3.0-only

#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include "mobile_work_abroad_calculator/Calculator.hpp"

#include <chrono>

SCENARIO( "Leftover working days can be calculated", "[Calculator]") {

	GIVEN( "contingent of 20 allowed working days" ) {
		using namespace std::literals::chrono_literals;
		constexpr const std::chrono::days allowed_days{20};
		constexpr const std::chrono::sys_days time_of_calculation = 2022y/02/18;

		WHEN( "No days have been worked abroad yet" ) {
			constexpr const std::vector<std::chrono::year_month_day> days_worked {};
			const Calculator calc(allowed_days, time_of_calculation, days_worked);

			THEN( "The leftover days are the complete contingent of 20" ) {
				REQUIRE( calc.LeftoverDays() == allowed_days );
			}
		}
		WHEN( "5 days have been worked abroad within the last 12 months" ) {
			const std::vector<std::chrono::year_month_day> days_worked {
					2021y/10/22,
					2021y/10/25,
					2021y/10/26,
					2021y/10/27,
					2021y/10/28,
			};
			const Calculator calc(allowed_days, time_of_calculation, days_worked);

			THEN( "There should be 15 days leftover" ) {
				REQUIRE( calc.LeftoverDays() == std::chrono::days{15} );
			}
		}
		WHEN( "5 days have been worked abroad before the last 12 months" ) {
			const std::vector<std::chrono::year_month_day> days_worked {
					2020y/10/22,
					2020y/10/25,
					2020y/10/26,
					2020y/10/27,
					2020y/10/28,
			};
			const Calculator calc(allowed_days, time_of_calculation, days_worked);

			THEN( "The leftover days are the complete contingent of 20" ) {
				REQUIRE( calc.LeftoverDays() == allowed_days );
			}
		}
		WHEN( "5 days have been worked abroad before the last 12 months, and 5 days within the last 12 months" ) {
			const std::vector<std::chrono::year_month_day> days_worked {
					2020y/10/22,
					2020y/10/25,
					2020y/10/26,
					2020y/10/27,
					2020y/10/28,
					2021y/10/22,
					2021y/10/25,
					2021y/10/26,
					2021y/10/27,
					2021y/10/28,
			};
			const Calculator calc(allowed_days, time_of_calculation, days_worked);

			THEN( "There should be 15 days leftover" ) {
				REQUIRE( calc.LeftoverDays() == std::chrono::days{15} );
			}
		}
		WHEN( "5 days working abroad within 12 months are mixed with 5 days of work abroad before 12 months" ) {
			const std::vector<std::chrono::year_month_day> days_worked {
					2020y/10/22,
					2021y/10/22,
					2020y/10/26,
					2021y/10/25,
					2020y/10/27,
					2020y/10/28,
					2021y/10/26,
					2020y/10/25,
					2021y/10/27,
					2021y/10/28,
			};
			const Calculator calc(allowed_days, time_of_calculation, days_worked);

			THEN( "There should be 15 days leftover" ) {
				REQUIRE( calc.LeftoverDays() == std::chrono::days{15} );
			}
		}
		WHEN( "A working day abroad is in the future" ) {
			const std::vector<std::chrono::year_month_day> days_worked {
				time_of_calculation + std::chrono::days{1}
			};
			const Calculator calc(allowed_days, time_of_calculation,
									days_worked);

			THEN("It shouldn't be discounted from the allowed working "
				   "days abroad") {
				REQUIRE(calc.LeftoverDays() == allowed_days);
			}
		}
		WHEN( "A working day within the last 12 months is listed more than once" ) {
			const std::vector<std::chrono::year_month_day> days_worked {
					2021y/10/28,
					2021y/10/28,
			};
			const Calculator calc(allowed_days, time_of_calculation,
					days_worked);

			THEN( "It is only accounted for once" ) {
				REQUIRE(calc.LeftoverDays() == std::chrono::days{19});
			}
		}
	}
}

SCENARIO( "Next possible date for working abroad can be found", "[Calculator]" ) {

	GIVEN( "5 contingent of allowed working days" ) {
		using namespace std::literals::chrono_literals;
		constexpr const std::chrono::days allowed_days{5};
		constexpr const std::chrono::sys_days time_of_calculation = 2022y/02/18;

		WHEN( "No days have been worked abroad yet" ) {
			const std::vector<std::chrono::year_month_day> days_worked {};
			const Calculator calc(allowed_days, time_of_calculation, days_worked);

			THEN( "The next possible date is today" ) {
				REQUIRE( calc.NextPossibility() == time_of_calculation );
			}
		}
		WHEN( "4 days have been worked abroad within the last 12 months" ) {
			const std::vector<std::chrono::year_month_day> days_worked {
					2021y/10/22,
					2021y/10/25,
					2021y/10/26,
					2021y/10/27,
			};
			const Calculator calc(allowed_days, time_of_calculation, days_worked);

			THEN( "The next possible date is today" ) {
				REQUIRE( calc.NextPossibility() == time_of_calculation );
			}
		}
		WHEN( "5 days have been worked abroad within the last 12 months" ) {
			const std::vector<std::chrono::year_month_day> days_worked {
					2021y/10/22,
					2021y/10/25,
					2021y/10/26,
					2021y/10/27,
					2021y/10/28,
			};
			const Calculator calc(allowed_days, time_of_calculation, days_worked);

			THEN( "The next possible date is 12 months after the first working day" ) {
				const auto expected_next_possible_working_day = 2022y/10/22;
				REQUIRE( calc.NextPossibility() == expected_next_possible_working_day );
			}
		}
		WHEN( "5 days have been worked abroad within the last 12 months and the input is unordered" ) {
			const std::vector<std::chrono::year_month_day> days_worked {
					2021y/10/28,
					2021y/10/25,
					2021y/10/26,
					2021y/10/27,
					2021y/10/22,
			};
			const Calculator calc(allowed_days, time_of_calculation, days_worked);

			THEN( "The next possible date is 12 months after the first working day" ) {
				const auto expected_next_possible_working_day = 2022y/10/22;
				REQUIRE( calc.NextPossibility() == expected_next_possible_working_day );
			}
		}
		WHEN( "3 days have been worked abroad within the last 12 months and 2 days are in the future" ) {
			const std::vector<std::chrono::year_month_day> days_worked {
					2021y/10/28,
					2021y/10/25,
					2021y/10/26,
					2022y/10/27,
					2022y/10/22,
			};
			const Calculator calc(allowed_days, time_of_calculation, days_worked);

			THEN( "The next possible working day is today" ) {
				REQUIRE( calc.NextPossibility() == time_of_calculation );
			}
		}
		WHEN( "5 days have been worked abroad and 3 of those within the last 12 months, the other 2 more than 12 months ago" ) {
			const std::vector<std::chrono::year_month_day> days_worked {
					2020y/10/22,
					2020y/10/25,
					2021y/10/26,
					2021y/10/27,
					2021y/10/28,
			};
			const Calculator calc(allowed_days, time_of_calculation, days_worked);

			THEN( "The next possible working day is today" ) {
				REQUIRE( calc.NextPossibility() == time_of_calculation );
			}
		}
		WHEN( "Today has been worked abroad" ) {
			const std::vector<std::chrono::year_month_day> days_worked {
				2022y/02/18
			};
			const Calculator calc(allowed_days, time_of_calculation, days_worked);

			THEN( "The next possible working day is tomorrow" ) {
				REQUIRE( calc.NextPossibility() == (time_of_calculation + std::chrono::days{1}) );
			}
		}
	}
}
