// SPDX-FileCopyrightText: 2022 Simon Barth <simon.barth@gmx.de>
//
// SPDX-License-Identifier: GPL-3.0-only

#ifndef _MOBILE_WORK_ABROAD_CALCULATOR_
#define _MOBILE_WORK_ABROAD_CALCULATOR_
#include "mobile_work_abroad_calculator_export.h"

#include <chrono>
#include <set>
#include <vector>

class MOBILE_WORK_ABROAD_CALCULATOR_EXPORT Calculator {
	public:
		Calculator(const std::chrono::days allowed_days_abroad,
										const std::chrono::sys_days target_date,
										const std::vector<std::chrono::year_month_day> days_worked_abroad
							);

		// Get number of remaining days to work abroad
		auto LeftoverDays() const -> std::chrono::days;
		// Get next possible point in time to work abroad
		auto NextPossibility() const -> std::chrono::year_month_day;
	private:
		const std::chrono::days mAllowedDays;
		const std::chrono::sys_days mTargetDate;
		const std::set<std::chrono::year_month_day> mDaysWorkedAbroad;
};

#endif
