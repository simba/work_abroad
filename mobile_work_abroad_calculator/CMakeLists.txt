# SPDX-FileCopyrightText: 2022 Simon Barth <simon.barth@gmx.de>
#
# SPDX-License-Identifier: GPL-3.0-only

cmake_minimum_required(VERSION 3.22)

project(MobileWorkAbroadCalculator
	VERSION
		0.0.0
	LANGUAGES
		CXX)

set(CMAKE_CXX_VISIBILITY_PRESET hidden)
set(CMKAE_VISIBILITY_INLINES_HIDDEN YES)

option(${PROJECT_NAME}_BUILD_TESTING "Build tests even if not top-level project" NO)


add_library(mobile_work_abroad_calculator)
target_compile_features(mobile_work_abroad_calculator
				PUBLIC
								cxx_std_20
)

# So we only export what we want to users
include(GenerateExportHeader)
generate_export_header(mobile_work_abroad_calculator)

target_sources(mobile_work_abroad_calculator
	PRIVATE
		src/calculator.cpp
)
target_include_directories(mobile_work_abroad_calculator
	PUBLIC
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/inc>
		$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
		$<INSTALL_INTERFACE:inc>
	PRIVATE
		src
)

#### Versioning

set_target_properties(mobile_work_abroad_calculator PROPERTIES
	SOVERSION 0
	VERSION   0
)

include(CMakePackageConfigHelpers)
write_basic_package_version_file(
	MobileWorkAbroadCalculatorConfigVersion.cmake
	VERSION 0.0.0
	COMPATIBILITY SameMajorVersion
)

#### Install
include(GNUInstallDirs)
set(MobileWorkAbroadCalculator_INSTALL_CMAKEDIR
	${CMAKE_INSTALL_LIBDIR}/cmake/MobileWorkAbroadCalculator
	CACHE STRING "Path to MobileWorkAbroadCalculator cmake files"
)
install(FILES
	MobileWorkAbroadCalculator.cmake
	${CMAKE_CURRENT_BINARY_DIR}/MobileWorkAbroadCalculatorConfigVersion.cmake
	DESTINATION ${MobileWorkAbroadCalculator_INSTALL_CMAKEDIR}
)
install(FILES
	${CMAKE_CURRENT_BUILD_DIR}/mobile_work_abroad_calculator_export.h
	TYPE INCLUDE
)
install(TARGETS mobile_work_abroad_calculator
	EXPORT MobileWorkAbroadCalculator_Targets
	INCLUDES DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
	RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
		COMPONENT MobileWorkAbroadCalculator_RunTime
	LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
		COMPONENT MobileWorkAbroadCalculator_RunTime
		NAMELINK_COMPONENT MobileWorkAbroadCalculator_Development
	ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
		COMPONENT MobileWorkAbroadCalculator_Development
)
install(EXPORT MobileWorkAbroadCalculator_Targets
	DESTINATION ${MobileWorkAbroadCalculator_INSTALL_CMAKEDIR}
	NAMESPACE MobileWorkAbroadCalculator::
	FILE MobileWorkAbroadCalculator-Targets.cmake
	COMPONENT MobileWorkAbroadCalculator_Development
)

# So users can always use "namespaced" version of target. If they use
# find_package or add_subdirectory.
set_target_properties(mobile_work_abroad_calculator PROPERTIES
	EXPORT_NAME mobile_work_abroad_calculator
)
add_library(MobileWorkAbroadCalculator::mobile_work_abroad_calculator ALIAS mobile_work_abroad_calculator)


#### Tests
if(PROJECT_IS_TOP_LEVEL)
    include(CTest)
endif()

if((PROJECT_IS_TOP_LEVEL OR ${PROJECT_NAME}_BUILD_TESTING) AND BUILD_TESTING)
	add_executable(mobile_work_abroad_calculator_test)
	target_link_libraries(mobile_work_abroad_calculator_test
		PRIVATE
			MobileWorkAbroadCalculator::mobile_work_abroad_calculator
	)
	add_test(
		NAME
			mobile_work_abroad_calculator_test
		COMMAND
			mobile_work_abroad_calculator_test
	)

	target_sources(mobile_work_abroad_calculator_test
		PRIVATE
			test/test-calculator.cpp
	)
	target_include_directories(mobile_work_abroad_calculator_test
		PRIVATE SYSTEM
			third-party
	)
endif()
