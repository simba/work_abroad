// SPDX-FileCopyrightText: 2022 Simon Barth <simon.barth@gmx.de>
//
// SPDX-License-Identifier: GPL-3.0-only

#include "mobile_work_abroad_calculator/Calculator.hpp"

#include <algorithm>
#include <chrono>
#include <unordered_set>
#include <vector>

Calculator::Calculator(
		const std::chrono::days allowed_days_abroad,
		const std::chrono::sys_days target_date,
		const std::vector<std::chrono::year_month_day> days_worked_abroad
)
: mAllowedDays(allowed_days_abroad)
, mTargetDate(target_date)
, mDaysWorkedAbroad(std::begin(days_worked_abroad), std::end(days_worked_abroad))
{}

/**
* @brief Function Object to determine if a day is within the last twelve month of a given date
*/
struct DayWithinLastTwelveMonths {
	DayWithinLastTwelveMonths(const std::chrono::sys_days target_date)
	: mTargetDate{target_date}
	{}

	// first in list + 12 months == first possible working day
	auto operator()(const std::chrono::year_month_day day) -> bool {
		if(!day.ok()) {
			return false;
		}
		const auto beginning_of_twelve_month_window = mTargetDate - std::chrono::months{12};
		const auto day_in_sys_days = std::chrono::sys_days{day};
		if ( (beginning_of_twelve_month_window <= day_in_sys_days)
			&& (day_in_sys_days <= mTargetDate)) {
			return true;
		} else {
			return false;
		}
	}

	const std::chrono::sys_days mTargetDate;
};


auto Calculator::LeftoverDays() const -> std::chrono::days
{
	const auto days_worked_within_last_twelve_months =
		std::count_if(std::cbegin(mDaysWorkedAbroad),
				std::cend(mDaysWorkedAbroad),
				DayWithinLastTwelveMonths{mTargetDate}
			);
	return mAllowedDays - std::chrono::days{days_worked_within_last_twelve_months};
}


auto Calculator::NextPossibility() const -> std::chrono::year_month_day
{
	// Check how many days are within 12 month window
	auto leftover_days = LeftoverDays();
	if(leftover_days > std::chrono::days{0})
	{
		const auto worked_on_target_date = [this](){
			const auto found_day = std::find(std::cbegin(mDaysWorkedAbroad), std::cend(mDaysWorkedAbroad),
						 mTargetDate);
			return found_day != std::cend(mDaysWorkedAbroad);
		};

		// If there are days left, but the day we're checking was worked on, the
		// next possible day is one day after the target date, otherwise the
		// next possible day is the target date.
		if(worked_on_target_date()) {
			return mTargetDate + std::chrono::days{1};
		} else {
			return mTargetDate;
		}
	}

	const auto first_day_within_window = std::find_if(
			std::cbegin(mDaysWorkedAbroad), std::cend(mDaysWorkedAbroad),
			DayWithinLastTwelveMonths{mTargetDate});

	return *first_day_within_window + std::chrono::months{12};
}
