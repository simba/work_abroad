<!--
SPDX-FileCopyrightText: 2022 Simon Barth <simon.barth@gmx.de>

SPDX-License-Identifier: GPL-3.0-only
-->

# Overview

Calculate how many days you are allowed to work abroad.

By law you have a limited contingent of working days. These days are applied to
a rolling 12 month window. This tool can tell you how many days you are still
allowed to work or when you are allowed to work again abroad.

To answer this question it needs the list of days already worked abroad and the
contingent of days you have.

# Development

To build this, you need a C++20 compiler. It's a CMake build system.

It provides two build options:

* _BUILD_TESTING_ to build the unit tests
* _MobileWorkAbroadCalculator_BUILD_TESTING_ to also build the unit test of the internal library


